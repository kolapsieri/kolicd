requirejs.config({
    baseUrl: '/public/js/lib',
    shim: {
        'backbone': {
            deps: ['underscore', 'jquery'],
            exports: 'backbone'
        },
        'underscore': {
            exports: '_'
        }
    }
});